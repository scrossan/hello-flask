#!/bin/bash

rm -rf target
mkdir target
pipenv run nosetests --with-xunit --xunit-file=target/nosetests.xml \
          --with-xcover --xcoverage-file=target/coverage/coverage.xml \
          --cover-package=hello_flask \
          --cover-erase \
          --cover-html-dir=target/coverage \
          --cover-html
