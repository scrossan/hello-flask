FROM python:3.7-alpine

RUN pip install pipenv
COPY hello_flask /app/hello_flask
COPY Pipfile* /tmp/

WORKDIR /tmp
RUN pipenv install --system --deploy

EXPOSE 8000

WORKDIR /app
CMD gunicorn hello_flask.app:app --bind 0.0.0.0 --access-logfile - --error-logfile -
