import os
from flask import Flask, jsonify, render_template, request

app = Flask(__name__)

# Config
current_dir = os.path.dirname(os.path.abspath(__file__))
config_file = os.path.join(current_dir, 'config', 'default.py')
app.config.from_pyfile(config_file)

# App
app.debug = True


@app.errorhandler(400)
def not_found_error(error):
    return jsonify(error=str(error)), 400


@app.errorhandler(500)
def internal_error(error):
    return jsonify(error=str(error)), 500


@app.route("/")
def hello():
    return render_template('index.html', greeting=app.config['GREETING'], headers=request.headers)


if __name__ == "__main__":
    app.run(threaded=True, host='0.0.0.0')
