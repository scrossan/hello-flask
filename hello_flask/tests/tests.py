import os
import unittest
from hello_flask import app


# Config
current_dir = os.path.dirname(os.path.abspath(__file__))
config_file = os.path.join(current_dir, '../config', 'default.py')
app.app.config.from_pyfile(config_file)

class TestCase(unittest.TestCase):

    def setUp(self):
        self.app = app.app.test_client()

    def test_200_response(self):
        rv = self.app.get('/')
        assert rv.status_code == 200
        assert "Hello, " + app.app.config['GREETING'] + "!" in rv.data.decode()

    def test_404_response(self):
        rv = self.app.get('/doesnt-exist')
        assert rv.status_code == 404
        assert "Not Found" in rv.data.decode()
